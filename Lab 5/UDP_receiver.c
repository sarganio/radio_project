/*
 ============================================================================
 Name        : UDP_receiver.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>

#define BUFFER_SIZE 1024

//error function
void error_exit(const char *msg)
 {
	perror(msg);
	exit(1);
}

int main(int argc, char *argv[]) {

	int sockfd,dataReceived = 0, numOfDatagrams = atoi(argv[3]), numOfByteReceived = 0;
	char buffer[BUFFER_SIZE+1];//last char '\0'
	struct sockaddr_in serverAddr;
	struct ip_mreq multicastAddr;


	if(argc != 4)
		error_exit("Not enough arguments..\n");

	//Multi-cast group setup
	multicastAddr.imr_multiaddr.s_addr = inet_addr(argv[1]);
	multicastAddr.imr_interface.s_addr = htonl(INADDR_ANY);

	//set server
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddr.sin_port = htons(atoi(argv[2]));

	//create socket
	if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
		error_exit("Error creating socket..\n");

	//Opening socket
	if(setsockopt(sockfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &multicastAddr,sizeof(multicastAddr))<0)
		error_exit("Error opening socket..\n");

	//Bind the socket
	if (bind(sockfd, (struct sockaddr*) &serverAddr, sizeof(serverAddr)) < 0){
		close(sockfd);
		error_exit("Error binding socket..\n");
	}

	//Receive data
	for(dataReceived = 0; dataReceived<BUFFER_SIZE*numOfDatagrams; dataReceived+=numOfByteReceived){

		numOfByteReceived = recvfrom(sockfd, buffer, BUFFER_SIZE, 0,(struct sockaddr *) &serverAddr, sizeof(serverAddr));
		if(numOfByteReceived <= 0){
			close(sockfd);
			error_exit("Error receiving data..\n");
		}
		printf("%s",buffer);
	}

	if(close(sockfd) < 0)
		error_exit("ERROR closing socket..\n");

	return 0;
}