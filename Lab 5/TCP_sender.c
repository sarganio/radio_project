/*
 ============================================================================
 Name        : TCP_receiver.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>

#define NUM_OF_ARGS 5
#define SIZE 1024
#define MAX_CONECTIONS 1

void badExit() {
	perror(errno);
	exit(1);
}

/*
 * arg[0] - reserved
 * arg[1] - ip address of the client
 * arg[2] - port number
 * arg[3] - num of segments
 * arg[4] - file's name to send
 */

int main(int argc, char* argv[]){
	char serverMessage[SIZE] = {0};
	int i;
	printf("TCP_sender starts...\n");
	//check if all arguments were inserted
	if(argc < NUM_OF_ARGS)
		badExit();

	//create socket
	int mySocket = socket(AF_INET,SOCK_STREAM,0);
	if (mySocket < 0)
		badExit();

	//define the server address
	struct sockaddr_in server_address;
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(atoi(argv[2]));
	server_address.sin_addr.s_addr = INADDR_ANY;

	//bind the socket to our specified IP and port
	if(bind(mySocket,(struct sockaddr *) &server_address,sizeof(server_address)) < 0)
		badExit();
	printf("Listening...\n");
	listen(mySocket,MAX_CONECTIONS);

	int clientSocket = accept(mySocket,NULL,NULL);
	if(clientSocket < 0)
		badExit();
	printf("Accepted client..\n");
	//open file to send
	FILE* fp = fopen(argv[4],"r");
	if(fp < 0){
		fclose(fp);
		badExit();
	}
	printf("Alice was opened..\n");
	//send data
	int numOfSeg = atoi(argv[3]);
	int byteToSend = 0;
	printf("Start sending data..\n");
	for(i = 0;i < numOfSeg;i++){
		//read the i'th segment data from file
		byteToSend = read(fileno(fp),serverMessage,SIZE);

		if(byteToSend < 0){
			fclose(fp);
			badExit();
		}
		//send the i'th segment
		byteToSend = send(clientSocket,serverMessage,sizeof(serverMessage),0);

		if(byteToSend != SIZE){
			fclose(fp);
			badExit();
		}
	}
	printf("Successful run :)\n ");
	//close the socket
	if(close(mySocket) < 0){
		fclose(fp);
		badExit();
	}
	return 0;

}//main