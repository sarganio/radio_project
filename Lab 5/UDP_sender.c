/*
 ============================================================================
 Name        : UDP_receiver.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>

#define BUFFER_SIZE 1024
#define NUM_OF_ARGS 5
#define TTL 15

//error function
void error_exit(const char *msg)
 {
	perror(msg);
	exit(1);
}

int main(int argc, char *argv[]) {
	if(argc != NUM_OF_ARGS)
		error_exit("Not enough arguments..\n");

	//set server's struct
	struct sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;				//IPv4
	serverAddr.sin_addr.s_addr = inet_addr(argv[1]);//ip address of the sender
	serverAddr.sin_port = htons(atoi(argv[2]));		//port number

	//create socket
	int sockfd;
	if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
		error_exit("Error creating socket..\n");

	//Opening socket
	int ttl = TTL;
	if(setsockopt(sockfd, IPPROTO_IP, IP_MULTICAST_TTL, &ttl,sizeof(ttl))<0){
		close(sockfd);
		error_exit("Error opening socket..\n");
	}
	//openning file to send
	FILE* fp = fopen(argv[4],"r");
	if(!fp){
		close(sockfd);
		error_exit("Failed open file..\n");
	}
	//send data
	int numOfDatagrams = atoi(argv[3]), dataSent = 0,numOfBytoSent = 0;
	char buffer[BUFFER_SIZE+1] = {0};//last char '\0'
	for(dataSent = 0; dataSent<BUFFER_SIZE*numOfDatagrams; dataSent+=numOfBytoSent){
		fscanf(fp,"%s",buffer);
		numOfBytoSent = sendto(sockfd,&buffer,sizeof(buffer),0,(struct sockaddr*)&serverAddr,sizeof(serverAddr));
		if(numOfBytoSent <= 0){
			fclose(fp);
			close(sockfd);
			error_exit("Error sending data..\n");
			
		}
	
	}

	if(close(sockfd) < 0 || fclose(fp) < 0)
		error_exit("ERROR closing socket");
	return 0;
}
