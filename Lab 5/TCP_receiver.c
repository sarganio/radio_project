/*
 ============================================================================
 Name        : TCP_receiver.c
 Author      :
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>

#define NUM_OF_ARGS 4
#define BUFFER_SIZE 1024
#define SIZE 1024
#define FILE_NAME "alice.txt"

void badExit() {
	printf("ERROR..\n");
	perror(errno);
	exit(0);
}

/*
 * arg[0] - reserved
 * arg[1] - ip address of server
 * arg[2] - port number
 */

int main(int argc, char* argv[]){

	int  socketFD,flag1 = 0,dataReceived = 0;
	struct sockaddr_in serverAddr;
	char buffer[BUFFER_SIZE];
	printf("Receiver Program started..\n");

	//check if all arguments were inserted
	if(argc != NUM_OF_ARGS)
		badExit();

	//set IP address port by reading args to hostnet struct
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(atoi(argv[2]));
	serverAddr.sin_addr.s_addr = inet_addr(argv[1]);

	//open socket
	socketFD = socket(AF_INET,SOCK_STREAM,0);
	if(socketFD < 0)
		badExit();

	//set sin_zero
	memset((char*)&serverAddr.sin_zero,0,sizeof(serverAddr));

	//connect
	printf("Setting connection..\n");
	//connect to socket
	if(connect(socketFD,(struct sockaddr *)&serverAddr,sizeof(serverAddr)) < 0)
		badExit();
	printf("connected to server..\n");
	//receiving data
	while(dataReceived != 10240 || flag1 == 0){
		flag1 = recv(socketFD,buffer,SIZE,0);
		dataReceived+=flag1;
		printf("%s",buffer);
		memset(buffer, 0, sizeof(buffer));
	}

	if(close(socketFD) < 0)
		badExit();
}//main