
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#define DEFAULT_VALUE 9999
#define HELLO_TIMEOUT 300000 //300 Milliseconds
#define ASKSONG_TIMEOUT 300000 //300 Milliseconds
#define UP_TIMEOUTE 300000 //300 Milliseconds

//cases of termination
#define ERROR_OPEN_TCP_SOKET 0
#define ERROR_CONNECT 1
#define ERROR_OPEN_UDP_SOKET 2
#define ERROR_FIRST_BIND 3
#define ERROR_SETSOKET 4
#define ERROR_RCV_UDP_CHUNK 5
#define PROPER_EXIT 6
#define ERROR_RCV_SONGNAME 7
#define ERROR_HELLO_TO 8
#define ERROR_ESTABLISHED_SELECT 9
#define ERROR_ASKSONG_TO 10
#define ERROR_UPSONG_TO 11
#define ERROR_SEND 12
#define ERROR_READ 13

enum CMessageNum {HELLO = 0,ASKSONG,UPSONG,CHANGE_STATION,EXIT};
enum SMessageNum {WELCOME = 0,ANNOUNCE = 0,PERMITSONG,INVALIDCOMMAND,NEWSTATIONS};

#define TRUE 1
#define UDP_CHUNK 1024
#define MAX_LEN_SONG_NAME 100
#define MAX_SONG_SIZE 10000000
#define MIN_SONG_SIZE	2000

//initialize default value for Hello message
#define INIT_HELLO(X) Hello X = {X.commandType = HELLO, X.reserved = 0}
//initialize default value for AskSong message
#define INIT_ASKSONG(X) AskSong X = {X.commandType = ASKSONG, X.stationNumber =  DEFAULT_VALUE}
//initialize default value for UpSong message
#define INIT_UPSONG(X) UpSong X = { X.commandType = UPSONG_PERMITSONG,X.songSize =  DEFAULT_VALUE,X.songNameSize =  DEFAULT_VALUE}

//initialize default value for Welcome message
#define INIT_WELCOME(X) Welcome X = {X.replyType = HELLO_WELCOME,X.multicastGroup = DEFAULT_VALUE,X.numStations = DEFAULT_VALUE,X.portNumber = DEFAULT_VALUE}
//initialize default value for Announce message
//#define INIT_ANNOUNCE(X,Y) Announce X = {X.replyType = 1,X.songNameSize = Y, X.songName = (char*)malloc(Y*sizeof(char))}
//initialize default value for PermitSong message
#define INIT_PERMITSONG(X) PermitSong X = {X.replyType = PERMITSONG, X.permit = DEFAULT_VALUE}
//initialize default value for InvalidCommand message
//#define INIT_INVALIDCOMMAND(X,Y) InvalidCommand X = {X.replyType = 3, X.replyStringSize = Y, X.replyString = (char*)malloc(Y*sizeof(char))}
//initialize default value for NewStations message
#define INIT_NEWSTATIONS(X) NewStations X = {X.replyType = NEWSTATIONS,X.newStationNumber = DEFAULT_VALUE}
//send + validity check
#define SEND(X,Y)  if ( write(serverSocket, &X, Y) < Y ) clean_exit(ERROR_SEND);
//read + validity check
#define READ(X,Y,Z)  if ( read(serverSocket, &X, Y) < Y || X.replyType != Z) clean_exit(ERROR_READ);

//Client's structs
#pragma pack(1)
typedef struct
{
	uint8_t commandType;//0
	uint16_t reserved;
}Hello;
#pragma pack(1)
typedef struct
{
	uint8_t commandType;//1
	uint16_t stationNumber;
}AskSong;
#pragma pack(1)
typedef struct
{
	uint8_t commandType;//2
	uint32_t songSize;//in bytes
	uint8_t songNameSize;
	char* songName;

}UpSong;

//Server's structs
#pragma pack(1)
typedef struct
{
	uint8_t replyType;//0
	uint32_t multicastGroup;
	uint16_t numStations;
	uint16_t portNumber;
}Welcome;
#pragma pack(1)
typedef struct
{
	uint8_t replyType;//1
	uint8_t songNameSize;
	char* songName;
}Announce;
#pragma pack(1)
typedef struct
{
	uint8_t replyType;//2
	uint8_t permit;
}PermitSong;
#pragma pack(1)
typedef struct
{
	uint8_t replyType;//3
	uint8_t replyStringSize;
	char* replyString;
}InvalidCommand;
#pragma pack(1)
typedef struct
{
	uint8_t replyType;//4
	uint16_t newStationNumber;
}NewStations;

/*functiond declaration*/
int check_error_select(int isNotTimeout);//
void clean_exit(int type);
void state_connection_established();//
int get_integer_from_user(const char* menu,int min, int max);
void get_NewStation_message();//
void ask_song();//
void init_announce(Announce* message,int nameSize);
void init_InvalidCommand(InvalidCommand* message, int stringSize);
int upload_song();//
void* listener(void* serverAddress);

//global variables
uint16_t numOfStations = DEFAULT_VALUE;
struct in_addr multicastAddress;//address of the first multicast group
uint serverSocket = 0;
FILE* fp;
uint32_t currentStation = 0;

/*
 * arg[0] - reserved
 * arg[1] - server's address
 * arg[2] - server's port number
 * arg[3] -
 */
int main(int argc,char* argv[]) {
	if (argc <= 3) {
		printf("Not enough arguments.\n");
		return 1;
	}
	printf("Preparing for connection with server:\n");

	//define the server address
	struct sockaddr_in serverAddress;
	serverAddress.sin_family = AF_INET;				//IPv4
	serverAddress.sin_port = htons(atoi(argv[2]));		//port number
	serverAddress.sin_addr.s_addr =inet_addr(argv[1]);	//server's address


	//create socket
	serverSocket = socket(AF_INET,SOCK_STREAM,0);
	if (serverSocket < 0)
		clean_exit(ERROR_OPEN_TCP_SOKET);
	printf("FD socket obtained.\n");

	printf("Send a connection request to serever.\n");
	//send request for a TCP connection
	int connectionFlag;
	connectionFlag = connect(serverSocket,(const struct sockaddr*)&serverAddress,sizeof(serverAddress));
	if( connectionFlag < 0)
		clean_exit(ERROR_CONNECT);
	printf("Connection established.\n");

	//send Hello message
	int size = sizeof(Hello);
	INIT_HELLO(hello);
    SEND(hello,size);
    printf("Hello message was sent to server.\n");

     //create a set of FDs to monitor
    fd_set monitored;
    FD_SET(serverSocket,&monitored);
    //create the timeout struct
    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = HELLO_TIMEOUT;

    printf("Waiting for a reply...\n");
    //wait for a reply (or time out)
    int isNotTimeout;
    isNotTimeout = select(serverSocket+1,&monitored,NULL,NULL,&timeout);
	if(check_error_select(isNotTimeout))
		clean_exit(ERROR_HELLO_TO);

    printf("Got a reply from the server.\n");
    //get a reply
    size = sizeof(Welcome);
    Welcome firstReply;
    READ(firstReply,size,WELCOME);
    //check if all bytes were received
    printf("The reply is a welcome message.\n");

    //set the information got from the server
    printf("Multicast address recieved: %d\n",firstReply.multicastGroup);
    printf("Multicast address recieved: %x\n",firstReply.multicastGroup);
    printf("Multicast address recieved: %u\n",firstReply.multicastGroup);
    multicastAddress.s_addr = firstReply.multicastGroup + argv[3];
    numOfStations =  firstReply.numStations;

    //create new thread to function as a multicast listener
    pthread_t listenerId;
    pthread_create(&listenerId,NULL,listener,(void*)&serverAddress);

    state_connection_established();
	return 0;
}
void clean_exit(int type){
	//print the cause of failure
	perror(strerror(errno));

	//check the cause of termination
	switch (type) {
		case ERROR_OPEN_TCP_SOKET:
		case ERROR_CONNECT:
		case ERROR_OPEN_UDP_SOKET:
		case ERROR_FIRST_BIND:
		case ERROR_RCV_UDP_CHUNK:
		case ERROR_HELLO_TO:
		case ERROR_ESTABLISHED_SELECT:
				break;
		default:
			break;
	}
	exit(1);
}
int check_error_select(int isNotTimeout) {
if (isNotTimeout <= 0) {
		if (!isNotTimeout)
			printf("request timed out.\n");

		return 1;
	}
return 0;
}
void* listener(void* _serverAddress){
	int udpSocket, listenningTo = currentStation;
	const struct sockaddr_in* serverAddress = (const struct sockaddr_in*)_serverAddress;
	char buffer[UDP_CHUNK+1] = {0};
	struct in_addr currentAddress;

	FILE* fp= popen("play -t mp3 -> /dev/null 2>%1","w");
	//create socket
	udpSocket = socket(AF_INET, SOCK_DGRAM, 0);
	if(udpSocket < 0)
		clean_exit(ERROR_OPEN_UDP_SOKET);
	//listen to the server's address and port
	 if(bind(udpSocket,serverAddress, sizeof(struct sockaddr_in)) < 0)
		 clean_exit(ERROR_FIRST_BIND);
	 while(TRUE){
		 currentAddress.s_addr = multicastAddress.s_addr + currentStation;
		//join the appropriate multicast group
		if(setsockopt(udpSocket, IPPROTO_IP, IP_ADD_MEMBERSHIP, &multicastAddress,sizeof(multicastAddress))<0)
			clean_exit(ERROR_SETSOKET);

		//listening to the chosen station
		while(listenningTo == currentStation){
			if ( recv(serverSocket, buffer, UDP_CHUNK,NULL) < UDP_CHUNK)
				clean_exit(ERROR_RCV_UDP_CHUNK);
			fwrite(buffer,sizeof(char),UDP_CHUNK,fp);
		}
	}
return NULL;

}

void state_connection_established(){
    int input = -1,isNotTimeout,keyboardFD = fileno(stdin);
    int maxFD = serverSocket > keyboardFD ? serverSocket:keyboardFD;

    const char* menu = "Welcome to CSE radio station!\n"
    		"1.Receive information about a chosen station\n"
    		"2.Upload a new song\n"
    		"3.Exit\n"
    		"Pleas insert the number of your choice:\n";
    //print the menu for the client
    printf("%s",menu);

    //insert the socket and the keyboard FD into set
    fd_set monitored;
    FD_ZERO(&monitored);
    FD_SET(serverSocket,&monitored);
    FD_SET(keyboardFD,&monitored);

	do{
	//monitor the socket and the keyboard
    isNotTimeout = select(maxFD+1,&monitored,NULL,NULL,NULL);
    if(check_error_select(isNotTimeout))
    	clean_exit(ERROR_ESTABLISHED_SELECT);

    //check which FD triggered the select() function
    input =-1;
    if(FD_ISSET(keyboardFD,&monitored))
    	input = get_integer_from_user(menu,ASKSONG,EXIT);
    else
    	get_NewStation_message(serverSocket);

    //check which operation do next
    if(input == UPSONG)
    	upload_song(serverSocket);
    if(input == ASKSONG)
    	ask_song(serverSocket);
    if(input == CHANGE_STATION)
    	currentStation = get_integer_from_user("Please enter the number of station you wish to listen to:",0,numOfStations-1);
	}while(input != EXIT);

	//going offline after receiving the EXIT command
	clean_exit(PROPER_EXIT);
}
int get_integer_from_user(const char* message,int min, int max){
	int input;
	do{
		scanf("%d",&input);
		//empty buffer
		while(getchar() != '\n');

		if(input <= max && input <= min)
			break;
		printf("Invalid input.\nPlease enter a new number in the proper range (from %d to %d).\n%s",min,max,message);
	}while(TRUE);

	return input;
}
void get_NewStation_message(){
	int size = sizeof(NewStations);
	NewStations newStations;
	READ(newStations,size,EXIT);
    printf("Received a NewStation message.\n");

    //update the number of stations
    numOfStations = newStations.newStationNumber;
}
void ask_song(){
	int size = sizeof(AskSong),input,isNotTimeout;
	const char* message = "Please insert the number of the station you wish to receive information about:\n";
	//define the set for select
	fd_set monitored;
	FD_ZERO(&monitored);
	FD_SET(serverSocket,&monitored);

    //create the timeout struct
    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = ASKSONG_TIMEOUT;

    INIT_ASKSONG(askSong);

    //get the number of the station from the user
	input = get_integer_from_user(message,0,numOfStations-1);

	//set the number of the station in the message
	askSong.stationNumber = input;
	//send the ask song message
	SEND(askSong,size);

	//wait for a reply (or timeout)
	isNotTimeout = select(serverSocket+1,&monitored,NULL,NULL,&timeout);
	if(check_error_select(isNotTimeout))
		clean_exit(ERROR_ASKSONG_TO);
	printf("Ask song message was sent.\n");

	//get a reply, announce song message
	Announce announce;
	size = sizeof(announce.replyType)+sizeof(announce.songNameSize);
	//receive the reply type and the size of the song's name
	READ(announce,size,ANNOUNCE);
	printf("received the first part of the announce message");
	//allocate memory for song's name
	init_announce(&announce,announce.songNameSize);

	//get the rest of the announce message
	if ( read(serverSocket, &announce+size, announce.songNameSize) < announce.songNameSize){
		clean_exit(ERROR_RCV_SONGNAME);
		free(announce.songName);
	}
	printf("Announce message was received.\n");

	//print the answer to the user's query
	printf("Station number %d is playing the song: %s",input,announce.songName);

	//free allocated memory
	free(announce.songName);
}
int upload_song(){
	char songName[MAX_LEN_SONG_NAME+1] = {0};

	//get the song's name from user
	do{
	songName[MAX_LEN_SONG_NAME] = 0;
	printf("Please enter the name of the song you wish to upload:\n");
	scanf("%s",songName);
	while(getchar() != '\n');
	if(songName[MAX_LEN_SONG_NAME] != 0)
		printf("Name is too long!\nPleas enter name upto %d chars",MAX_LEN_SONG_NAME);
	}while(songName[MAX_LEN_SONG_NAME] != 0);
	FILE* fdSong = fopen(songName,"r");
	if(!fdSong)
		clean_exit(serverSocket);

	//set upSong message
	UpSong upSong;
	upSong.commandType = UPSONG;
	upSong.songNameSize = strlen(songName);
	//get the song's size
	fseek(fdSong,NULL,SEEK_END);
	upSong.songSize = ftell(fdSong);
	fseek(fdSong,NULL,SEEK_SET);
	upSong.songName = songName;

	//check if song size is valid
	if(upSong.songSize < MIN_SONG_SIZE || upSong.songSize > MAX_SONG_SIZE)
		return 0;
	fd_set monitored;
	FD_ZERO(&monitored);
	FD_SET(serverSocket,&monitored);

    //create the timeout struct
    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = UP_TIMEOUTE;

    int isNotTimeout;
	isNotTimeout = select(serverSocket+1,&monitored,NULL,NULL,&timeout);
	if(check_error_select(isNotTimeout))
		clean_exit(ERROR_UPSONG_TO);

	INIT_PERMITSONG(permitSong);
	int size = sizeof(PermitSong);
	READ(permitSong,size,PERMITSONG);

	if(permitSong.permit != 1)
		return 1;



}
void init_announce(Announce* message,int nameSize) {
	message->replyType = ANNOUNCE;
	message->songNameSize = nameSize;
	message->songName = (char*)malloc(nameSize * sizeof(char));
}

void init_InvalidCommand(InvalidCommand* message, int stringSize) {
	message->replyType = INVALIDCOMMAND;
	message->replyStringSize = stringSize;
	message->replyString = (char*)malloc(stringSize * sizeof(char));
}

