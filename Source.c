
#include <stdio.h>
#include <stdint.h>

#define DEFAULT_VALUE 9999

//initialize default value for Hello message
#define INIT_HELLO(X) Hello X = {X.commandType = 0, X.reserved = 0}
//initialize default value for AskSong message
#define INIT_ASKSONG(X) AskSong X = {X.commandType = 1, X.stationNumber =  DEFAULT_VALUE}
//initialize default value for UpSong message
#define INIT_UPSONG(X) UpSong X = { X.commandType = 2,X.songSize =  DEFAULT_VALUE,X.songNameSize =  DEFAULT_VALUE}

//initialize default value for Welcome message
#define INIT_WELCOME(X) Welcome X = {X.replyType = 0,X.multicastGroup = DEFAULT_VALUE,X.numStations = DEFAULT_VALUE,X.portNumber = DEFAULT_VALUE}
//initialize default value for Announce message
//#define INIT_ANNOUNCE(X,Y) Announce X = {X.replyType = 1,X.songNameSize = Y, X.songName = (char*)malloc(Y*sizeof(char))}
//initialize default value for PermitSong message
#define INIT_PERMITSONG(X) PermitSong X = {X.replyType = 2, X.permit = DEFAULT_VALUE}
//initialize default value for InvalidCommand message
//#define INIT_INVALIDCOMMAND(X,Y) InvalidCommand X = {X.replyType = 3, X.replyStringSize = Y, X.replyString = (char*)malloc(Y*sizeof(char))}
//initialize default value for NewStations message
#define INIT_NEWSTATIONS(X) NewStations X = {X.replyType = 4,X.newStationNumber = DEFAULT_VALUE}

//Client's structs
#pragma pack(1)
typedef struct
{
	uint16_t reserved;
	uint8_t commandType;//0
}Hello;
#pragma pack(1)
typedef struct 
{
	uint16_t stationNumber;
	uint8_t commandType;//1
}AskSong;
#pragma pack(1)
typedef struct 
{
	uint32_t songSize;//in bytes
	uint8_t songNameSize;
	uint8_t commandType;//2
	char* songName;

}UpSong;

//Server's structs
#pragma pack(1)
typedef struct 
{
	uint32_t multicastGroup;
	uint16_t numStations;
	uint16_t portNumber;
	uint8_t replyType;//0
}Welcome;
#pragma pack(1)
typedef struct 
{
	uint8_t replyType;//1
	uint8_t songNameSize;
	char* songName;
}Announce;
#pragma pack(1)
typedef struct 
{
	uint8_t replyType;//2
	uint8_t permit;
}PermitSong;
#pragma pack(1)
typedef struct 
{
	uint8_t replyType;//3
	uint8_t replyStringSize;
	char* replyString;
}InvalidCommand;
#pragma pack(1)
typedef struct
{
	uint16_t newStationNumber;
	uint8_t replyType;//4
}NewStations;

/*functiond declaration*/
int init_announce(Announce*,int);
int init_InvalidCommand(InvalidCommand*,int);
void clean_exit();
int main() {
	
	return 0;
}
int init_announce(Announce* message,int nameSize) {
	message->replyType = 1;
	message->songNameSize = nameSize;
	message->songName = (char*)malloc(nameSize * sizeof(char));
	return message->songName;
}
int init_InvalidCommand(InvalidCommand* message, int stringSize) {
	message->replyType = 3;
	message->replyStringSize = stringSize;
	message->replyString = (char*)malloc(stringSize * sizeof(char));
	return message->replyString;
}